###################################################
## Código para preparar a tabela `experimento02` ##
###################################################
library(dplyr)
library(readxl)
library(janitor)

devtools::load_all()

sacadasexperimento2 <- read_excel(
  system.file("extdata", "sacadasexperimento2.xlsx",
              package = "eyetrackingRachel2023"),
  na = "."
)

## Normaliza os nomes das colunas
experimento02 <- sacadasexperimento2 |>
  clean_names()

## Corrige IDs dos participantes
experimento02 <- experimento02 |>
  mutate(
    recording_session_label = case_when( # Corrige id dos participantes
      recording_session_label == "T2P04" ~ "T02P04",
      recording_session_label == "T2P05t" ~ "T02P05",
      recording_session_label == "T2P06" ~ "T02P06",
      recording_session_label == "T2P07" ~ "T02P07",
      recording_session_label == "T2P08" ~ "T02P08",      
      recording_session_label == "T1P05" ~ "T01P05",
      recording_session_label == "P39E02" ~ "T02P39",
      recording_session_label == "P41E02" ~ "T02P41",
      recording_session_label == "P42E02" ~ "T02P42",
      recording_session_label == "P43E02" ~ "T02P43",
      recording_session_label == "P44E02" ~ "T02P44",
      recording_session_label == "P45E02" ~ "T02P45",
      TRUE ~ recording_session_label
    )
  )

## Os seguintes participantes devem ser desconsiderados
experimento02 <- experimento02 |>
  filter(!recording_session_label %in% c("T02P34"))

## Salva dataframe no pacote
usethis::use_data(experimento02, overwrite = TRUE)
