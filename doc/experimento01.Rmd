---
title: "Experimento 1"
output: 
  bookdown::pdf_document2:
    latex_engine: xelatex
    keep_tex: true
    toc_depth: 3
header-includes:
  - \usepackage{icomma}
  - \usepackage{float}
fontsize: 12pt
lang: pt-BR
geometry: margin=1in
vignette: >
  %\VignetteIndexEntry{experimento01}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
  %\VignetteDepends{tidyverse}
  %\VignetteDepends{devtools}
  %\VignetteDepends{lme4}
  %\VignetteDepends{lmerTest}
  %\VignetteDepends{broom.mixed}
  %\VignetteDepends{kableExtra}
  %\VignetteDepends{ggthemes}
  %\VignetteDepends{ggsci}
  %\VignetteDepends{rgl}
---

```{r, include = FALSE}
library(rgl)

knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  echo = FALSE,
  message = FALSE,
  warning = FALSE
)

options(rgl.useNULL = TRUE)
setupKnitr(autoprint = TRUE)
```

```{r setup}
devtools::load_all()
library(eyetrackingRachel2023)
library(tidyverse)
library(lme4)
library(lmerTest)
library(ggthemes)
library(ggsci)
library(broom.mixed)

print_tbl <- function(x, caption = "") {
  x |>
    knitr::kable(
      caption = caption,
      booktabs = TRUE
    ) |>
    kableExtra::kable_styling(
      latex_options = c("HOLD_position", "scale_down")
    )
}
```

# Introdução

Este relatório tem por objetivo auxiliar na investigação do comportamento dos grupos `Controle`, composto por indivíduos letrados, e `Experimental`, formado por indivíduos não letrados, com base no `Experimento 1`, focado na movimentação ocular no eixo horizontal, especificamente as _fixações_.

Busca-se compreender se a habilidade de ler da esquerda para a direita é um traço biológico ou predominantemente influenciada por fatores culturais. Para isso, serão analisados os resultados obtidos neste experimento e no [experimento 2](experimento02.pdf) para identificar padrões de comportamento.

O _Experimento 1_ consiste em exibir 4 imagens para os participantes, onde eles relacionam uma imagem com a informação fornecida. A cada participante são apresentados 27 itens experimentais, porém são considerados somente a partir do quarto, pois os 3 primeiros têm a finalidade de treino.

```{r}
#' Tabela a ser utilizada nas análises
df1 <- experimento01 |>
  left_join(
    participantes |>  
      filter(experimento == 1) |>
      select(id, grupo),
    by = c("recording_session_label" = "id"))
```

# Análise Descritiva

Observa-se na tabela \@ref(tab:tbl-porcentagem-escolha-imagem) que o grupo `Controle` obteve mais sucesso nas escolhas da imagem alvo do que o grupo `Experimental`, apresentando 51% de sucesso. O grupo `Controle` concentra aproximadamente 95% das escolhas nas imagens _alvo_ e _distratora_, enquanto no grupo `Experimental` essa taxa é de 84,6%, sendo a porcentagem de escolha da imagem _distratora_ maior do que a da imagem _alvo_. Outra observação é a taxa de escolha da imagem _não-relacionada_ pelo grupo `Experimental` que é consideravelmente maior do que a do grupo `Controle`.
```{r tbl-porcentagem-escolha-imagem}
tbl2 <- function () {
  tmp <- df1 |>
    mutate(
      image_chosen = case_when(
        image_chosen == "competitor" ~ "Competidora",
        image_chosen == "distractor" ~ "Distratora",
        image_chosen == "target" ~ "Alvo",
        image_chosen == "unrelated" ~ "Não-relacionada",
        TRUE ~ image_chosen
      )
    )

  total_geral <- tmp |>
    group_by(image_chosen) |>
    summarize(cnt = n()) |>
    mutate("Total Geral" = 100.0 * (cnt / sum(cnt))) |>
    select(-c(cnt))

  tmp |>
    group_by(grupo, image_chosen) |>
    summarize(cnt = n()) |>
    mutate(porcentagem = round(100.0 * (cnt / sum(cnt)), 2)) |>
    rename("imagem_escolhida" = "image_chosen") |>
    select(-c(cnt)) |>
    pivot_wider(names_from = "grupo", values_from = porcentagem) |>
    left_join(total_geral, by = c( "imagem_escolhida" = "image_chosen")) |>
    rename(
      "Imagem" = "imagem_escolhida"    
    )
}

tbl2() |>
  print_tbl(caption = "Porcentagem de escolha das imagens")
```

A seguir, vamos analisar o *custo de processamento* dentro do experimento para os dois grupos. Por *custo de processamento*, entende-se uma das seguintes medidas:

* tempo de fixação: coluna `ia_dwell_time`,
* número de fixações: coluna `ia_fixation_count`,
* tempo de reação: coluna `rt`.

Assim, temos as relações:

* tempo de fixação maior/menor $\implies$ maior/menor custo de processamento
* número de fixações maior/menor $\implies$ maior/menor custo de processamento
* tempo de reação maior/menor $\implies$ maior/menor custo de processamento

Analisando o tempos de fixação de cada imagem por grupo, apresentados na tabela \@ref(tab:tbl-tempo-fixacao), temos que todas as médias dos tempos de fixação do grupo `Experimental` são superiores às médias do grupo `Controle` em todas as imagens, principalmente na imagem _Alvo_, onde a diferença percentual entre as médias é de 39,4%. Isto sugere que o custo de processamento para o grupo `Controle` é menor na escolha das imagens em relação ao grupo `Experimental`. 
```{r tbl-tempo-fixacao}
df1 |>
  mutate(
    image_chosen = case_when(
        image_chosen == "competitor" ~ "Competidora",
        image_chosen == "distractor" ~ "Distratora",
        image_chosen == "target" ~ "Alvo",
        image_chosen == "unrelated" ~ "Não-relacionada",
        TRUE ~ image_chosen
      )
  ) |>   
  group_by(grupo, image_chosen) |>
  summarize(
    Media = mean(ia_dwell_time),
    Variancia = var(ia_dwell_time),
    `Desvio Padrão` = sd(ia_dwell_time)
  ) |>
  ungroup() |>
  pivot_wider(names_from = "grupo", values_from = c(Media, Variancia, `Desvio Padrão`)) |>
  rename(
    "Média_c" = "Media_Controle",
    "Var_c" = "Variancia_Controle",
    "Dev_c" = "Desvio Padrão_Controle",
    "Média_e" = "Media_Experimental",
    "Var_e" = "Variancia_Experimental",
    "Dev_e" = "Desvio Padrão_Experimental"
  ) |>
  mutate(dif_perc = 100*abs(`Média_c` - `Média_e`)/`Média_c`) |>
  rename(
    "Imagem" = "image_chosen",
  ) |>
  select(Imagem, `Média_c`, `Var_c`, `Dev_c`, `Média_e`, `Var_e`, `Dev_e`, dif_perc) |>
  print_tbl(caption = "Medidas dos tempos de fixação")
```

Podemos segregar também a análise acima pelo tipo de voz do verbo, como mostrado na tabela \@ref(tab:tbl-tempo-fixacao-por-voz):
```{r tbl-tempo-fixacao-por-voz}
df1 |>
  mutate(
    voice = case_when(
      voice == "A" ~ "Ativa",
      voice == "P" ~ "Passiva",
      TRUE ~ voice
    ),
    image_chosen = case_when(
        image_chosen == "competitor" ~ "Competidora",
        image_chosen == "distractor" ~ "Distratora",
        image_chosen == "target" ~ "Alvo",
        image_chosen == "unrelated" ~ "Não-relacionada",
        TRUE ~ image_chosen
      )
  ) |>   
  group_by(grupo, voice, image_chosen) |>
  summarize(
    Media = mean(ia_dwell_time),
    Variancia = var(ia_dwell_time),
    `Desvio Padrão` = sd(ia_dwell_time)
  ) |>
  ungroup() |>
  pivot_wider(names_from = "grupo", values_from = c(Media, Variancia, `Desvio Padrão`)) |>
  rename(
    "Média_c" = "Media_Controle",
    "Var_c" = "Variancia_Controle",
    "Dev_c" = "Desvio Padrão_Controle",
    "Média_e" = "Media_Experimental",
    "Var_e" = "Variancia_Experimental",
    "Dev_e" = "Desvio Padrão_Experimental"
  ) |>
  mutate(dif_perc = 100*abs(`Média_c` - `Média_e`)/`Média_c`) |>
  rename(
    "Voz" = "voice",
    "Imagem" = "image_chosen"
  ) |>
  select(Voz, Imagem, `Média_c`, `Var_c`, `Dev_c`, `Média_e`, `Var_e`, `Dev_e`, dif_perc) |>
  print_tbl(caption = "Medidas descritivas do tempo de fixação, por grupo e voz do verbo.")
```
Podemos observar que o grupo `Experimental` apresenta em geral um tempo de fixação médio maior do que o grupo `Controle`, exceto nas frases na voz _Ativa_ com imagem _Não-relacionada_ e em frases na voz _Passiva_ com imagem _Competidora_.

Analisando o *número de fixações*, na figura \@ref(fig:fixacao-grupo) pode-se notar que o número médio de fixações é maior no grupo _Experimental_, sugerindo que o custo de processamento é menor para o grupo _Controle_. A diferença é mais notável em sentenças na voz _ativa_, como mostrado na figura \@ref(fig:fixacao-por-grupo-voz).

```{r fixacao-grupo, fig.show = "hold", fig.dim = c(7, 3.5), fig.cap="Fixações nas imagens por grupos"}
df1 |>
  mutate(
    ia_label = case_when(
      ia_label == "COMPETITOR" ~ "Competidora",
      ia_label == "TARGET" ~ "Alvo",
      ia_label == "DISTRACTOR" ~ "Distratora",
      ia_label == "UNRELATED" ~ "Não-relacionada",
      TRUE ~ ia_label
    )
  ) |>
  ggplot(mapping = aes(x = ia_label, y = ia_fixation_count, fill = grupo)) +
  geom_errorbar(
    stat = "boxplot",
    position = position_dodge(width = 0.6),
    width = 0.2
  ) +
  geom_boxplot(width = 0.6, outlier.size = 1.6) +
  scale_y_continuous(breaks = seq(0, 45, 5)) +
  theme_economist() + scale_fill_economist() +
  labs(
    x = "Tipo da imagem",
    y = "Número de fixações",
    fill = "Grupo"
    ) +
  theme(
    axis.title.y = element_text(vjust = +3.0),
    axis.title.x = element_text(vjust = -2.0)
  ) 
```

```{r fixacao-por-grupo-voz, fig.show = "hold", fig.dim = c(9, 5), fig.cap="Fixações nas imagens por grupos e tipo de voz verbal"}
df1 |>
  mutate(
    voice = case_when(
      voice == "A" ~ "Ativa",
      voice == "P" ~ "Passiva",
      TRUE ~ voice
    ),
    ia_label = case_when(
      ia_label == "COMPETITOR" ~ "Competidora",
      ia_label == "TARGET" ~ "Alvo",
      ia_label == "DISTRACTOR" ~ "Distratora",
      ia_label == "UNRELATED" ~ "Não-relacionada",
      TRUE ~ ia_label
    )
  ) |>
  ggplot(mapping = aes(x = ia_label, y = ia_fixation_count, fill = grupo)) +
  geom_errorbar(
    stat = "boxplot",
    position = position_dodge(width = 0.6),
    width = 0.2
  ) +
  geom_boxplot(width = 0.6, outlier.size = 1.6) +
  theme_economist() + scale_fill_economist() +
  scale_y_continuous(breaks = seq(0, 45, 5)) +
  labs(
    x = "Tipo da imagem",
    y = "Número de fixações",
    fill = "Grupo"
    ) +
  theme(
    axis.title.y = element_text(vjust = +3.0),
    axis.title.x = element_text(vjust = -2.0)
  ) +
  facet_wrap(~voice)
```

Analisando a média do tempo de reação, presente na figura \@ref(fig:tempo-reacao), vemos uma grande variação nos testes iniciais do grupo `Experimental` até a metade do teste, que é onde começa uma certa estabilização nos tempos. O grupo `Controle` tem uma média bem estabilizada desde o início do teste, mostrando uma melhor adaptação após as tentativas piloto. Em geral a média do tempo de reação do grupo `Controle` é menor do que o grupo `Experimental` mas com o andamento do teste essas médias vão se estabilizando e passam a seguir um mesmo padrão de comportamento. Isto sugere um custo de processamento maior para o grupo `Experimental`, que levam mais tempo na escolha da imagem. Essa conclusão é mantida para os dois tipos de voz verbal, como mostrado na figura \@ref(fig:tempo-reacao-voz).

```{r tempo-reacao, fig.cap="Tempo de reação por teste", fig.show = "hold", fig.dim = c(9, 5)}
#' Tempo de reação por teste
df1 |>
  group_by(trial_index, grupo) |>
  summarize(media_tempo_reacao = mean(rt)) |>
  ungroup() |>
  ggplot(aes(x = trial_index, y= media_tempo_reacao , color = grupo)) +
  geom_point() +
  stat_smooth() +
  scale_x_continuous(breaks = 4:27) +
  labs(
    x = "Teste",
    y = "Média de tempo de reação (ms)",
    color = "Grupo"
  ) +
  theme_economist() + scale_colour_economist() +
  theme(
    axis.title.y = element_text(vjust = +3.0),
    axis.title.x = element_text(vjust = -2.0)
  ) 
```

```{r tempo-reacao-voz, fig.cap="Tempo de reação por teste, segregado por voz", fig.show = "hold", fig.dim = c(11, 6)}
#' Tempo de reação por teste, segregado por voz
df1 |>
  mutate(
    voice = case_when(
      voice == "A" ~ "Ativa",
      voice == "P" ~ "Passiva",
      TRUE ~ voice
    ),
    ) |>
  group_by(trial_index, voice, grupo) |>
  summarize(media_tempo_reacao = mean(rt)) |>
  ungroup() |>
  ggplot(aes(x = trial_index, y= media_tempo_reacao , color = grupo)) +
  geom_point() + #line(linewidth = 1.5) +
  stat_smooth() +
  facet_wrap(~voice, ncol=2) +
  scale_x_continuous(breaks = 4:27) +
  labs(
    x = "Teste",
    y = "Média de tempo de reação (ms)",
    color = "Grupo"
  ) +
  theme_economist() + scale_colour_economist() +
  theme(
    axis.title.y = element_text(vjust = +3.0),
    axis.title.x = element_text(vjust = -2.0)
  ) 
```

Ao observarmos o tempo médio de fixação na tabela \@ref(tab:tbl-tempo-fixacao-por-voz), o grupo `Experimental` apresenta um tempo médio de fixação $47,7\%$ maior em relação ao grupo `Controle` para o processamento da imagem competidora em sentenças ativas. Também, existe um custo de processamento menor para a imagem competidora em sentenças na voz passiva.

**Obs**: Apesar de da diferenca grande para o caso, é possível que o grande valor se deva pelo _outlier_. Contudo, mesmo removendo essa observação o conclusão qualitativa não é afetada.

Na figuras \@ref(fig:tempo-fixacao-teste) e \@ref(fig:tempo-fixacao-voz) observamos que o grupo `Experimental` tem uma média maior no tempo de fixação nas figuras do que o grupo `Controle`, principalmente na parte inicial do experimento. É visto também que as médias de fixações do grupo `Controle` tem uma certa estabilidade durante o teste enquanto a do grupo `Experimental` varia bastante ao longo do teste.

```{r tempo-fixacao-teste, fig.cap="Tempo de fixação por teste", fig.show = "hold", fig.dim = c(11, 6)}
#' Tempo de fixação por teste
df1 |>
  group_by(trial_index, grupo) |>
  summarize(media_tempo_fixacao = mean(ia_dwell_time)) |>
  ungroup() |>
  ggplot(aes(x = trial_index, y= media_tempo_fixacao , color = grupo)) +
  geom_point() +
  stat_smooth() +
  scale_x_continuous(breaks = 4:27) +
  labs(
    x = "Teste",
    y = "Média de tempo de fixação (ms)",
    color = "Grupo"
  ) +
  theme_economist() + scale_colour_economist() +
  theme(
    axis.title.y = element_text(vjust = +3.0),
    axis.title.x = element_text(vjust = -2.0)
  ) 
```

```{r tempo-fixacao-voz, fig.cap="Tempo de fixação por teste, segregado por voz", fig.show = "hold", fig.dim = c(11, 6)}
#' Tempo de fixação por teste, segregado por voz
df1 |>
  mutate(
    voice = case_when(
      voice == "A" ~ "Ativa",
      voice == "P" ~ "Passiva",
      TRUE ~ voice
    ),
    ) |>
  group_by(trial_index, voice, grupo) |>
  summarize(media_tempo_fixacao = mean(ia_dwell_time)) |>
  ungroup() |>
  ggplot(aes(x = trial_index, y = media_tempo_fixacao , color = grupo)) +
  geom_point() + 
  stat_smooth() +
  facet_wrap(~voice, ncol=2) +
  scale_x_continuous(breaks = 4:27) +
  labs(
    x = "Teste",
    y = "Média de tempo de fixação (ms)",
    color = "Grupo"
  ) +
  theme_economist() + scale_colour_economist() +
  theme(
    axis.title.y = element_text(vjust = +3.0),
    axis.title.x = element_text(vjust = -2.0)
  ) 
```

```{r, fig.cap="Tempo de reação por grupo, segregado por voz", fig.show = "hold", fig.dim = c(11, 6)}
df1 |>
  mutate(
    image_chosen = case_when(
      image_chosen == "competitor" ~ "Competidora",
      image_chosen == "target" ~ "Alvo",
      image_chosen == "distractor" ~ "Distratora",
      image_chosen == "unrelated" ~ "Não-relacionada",
      TRUE ~ image_chosen     
    ),
    voice = case_when(
      voice == "A" ~ "Ativa",
      voice == "P" ~ "Passiva",
      TRUE ~ voice
    )    
  ) |>
  group_by(voice, grupo, image_chosen) |>
  summarize(media = mean(rt)) |>
  ggplot(aes(y = media, x = image_chosen, group=voice, fill=voice)) +
  geom_bar(stat="identity") +
  theme_economist() + scale_fill_economist() +
  facet_wrap(~grupo) +
  labs(
    fill = "Voz",
    x = "Imagem Escolhida",
    y = "Media de tempo de reação (ms)"
  ) +
  theme(
    axis.title.y = element_text(vjust = +3.0),
    axis.title.x = element_text(vjust = -2.0),
    axis.text.x = element_text(angle = 90, vjust = 0.5, hjust = 1)  
  )
```

```{r, fig.cap="Tempo de fixação por grupo, segregado por voz", fig.show = "hold", fig.dim = c(11, 6)}
df1 |>
  mutate(
    image_chosen = case_when(
      image_chosen == "competitor" ~ "Competidora",
      image_chosen == "target" ~ "Alvo",
      image_chosen == "distractor" ~ "Distratora",
      image_chosen == "unrelated" ~ "Não-relacionada",
      TRUE ~ image_chosen     
    ),
    voice = case_when(
      voice == "A" ~ "Ativa",
      voice == "P" ~ "Passiva",
      TRUE ~ voice
    )    
  ) |>
  group_by(voice, grupo, image_chosen) |>
  summarize(media = mean(ia_dwell_time)) |>
  ggplot(aes(y = media, x = image_chosen, group=voice,fill=voice)) +
  geom_bar(stat="identity") +
  theme_economist() + scale_fill_economist() +
  facet_wrap(~grupo) +
  labs(
    fill = "Voz",
    y = "Média de tempo de fixação (ms)",
    x = "Imagem Escolhida"
  ) +  
  theme(
    axis.title.y = element_text(vjust = +3.0),
    axis.title.x = element_text(vjust = -2.0),
    axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1),    
    )
```

```{r }
hipotese3 <- df1 |>  
  mutate(
    tipo = case_when(
      audiofile == "odiado.wav" ~ 3,
      audiofile == "odiou.wav" ~ 3,
      audiofile == "aborrecido.wav" ~ 3,
      audiofile == "aborreceu.wav" ~ 3,
      audiofile == "amado.wav" ~ 3,
      audiofile == "amou.wav" ~ 3,
      audiofile == "alegrado.wav" ~ 3,
      audiofile == "alegrou.wav" ~ 3,
      audiofile == "empurrado.wav" ~ 1,
      audiofile == "empurrou.wav" ~ 1,
      audiofile == "chutado.wav" ~ 1,
      audiofile == "chutou.wav" ~ 1,
      audiofile == "afastado.wav" ~ 1,
      audiofile == "afastou.wav" ~ 1,
      audiofile == "esmurrado.wav" ~ 1,
      audiofile == "esmurrou.wav" ~ 1,
      audiofile == "puxado.wav" ~ 2,
      audiofile == "puxou.wav" ~ 2,
      audiofile == "arrastado.wav" ~ 2,
      audiofile == "arrastou.wav" ~ 2,
      audiofile == "trazido.wav" ~ 2,
      audiofile == "trouxe.wav" ~ 2,
      audiofile == "aproximado.wav" ~ 2,
      audiofile == "aproximou.wav" ~ 2,
      TRUE ~ -1
    ),
    voice = case_when(
      voice == "A" ~ "Ativa",
      voice == "P" ~ "Passiva",
      TRUE ~ voice
    )
  )

hipotese3 |>
  group_by(grupo, voice, tipo) |>
  summarize(
    Media = mean(ia_dwell_time),
    Variancia = var(ia_dwell_time),
    `Desvio Padrão` = sd(ia_dwell_time)
  ) |>
  ungroup() |>
  filter(tipo != -1) |>
  pivot_wider(names_from = "grupo", values_from = c(Media, Variancia, `Desvio Padrão`)) |>
  rename(
    "Média_c" = "Media_Controle",
    "Var_c" = "Variancia_Controle",
    "Dev_c" = "Desvio Padrão_Controle",
    "Média_e" = "Media_Experimental",
    "Var_e" = "Variancia_Experimental",
    "Dev_e" = "Desvio Padrão_Experimental"
  ) |>
  mutate(dif_m_perc = 100*abs(`Média_c` - `Média_e`)/`Média_c`) |>
  print_tbl(caption = "Medidas descritivas para o tempo de fixação de acordo com o tipo do verbo e a voz")
```

```{r}
hipotese3 |>
  group_by(grupo, tipo, voice) |>  
  summarize(custo_processamento = mean(ia_dwell_time)) |>
  filter(tipo %in% 1:3) |>
  ggplot(aes(x = tipo, y = custo_processamento, fill=grupo)) +
  geom_bar(stat="identity", position="dodge") +
  facet_wrap(~voice) +
  labs(
    x = "Tipo de verbo",
    y = "Custo de processamento (ms)",
    fill = "Grupo"
  ) +
  theme_economist() + scale_fill_economist() +
  theme(
    axis.title.y = element_text(vjust = +3.0),
    axis.title.x = element_text(vjust = -2.0)
  ) 
```

# Análise Longitudinal

A _análise longitudinal_ é um método estatístico que visa analisar as variações nas caraterísticas dos mesmos
elementos amostrais ao longo de um período de tempo. Para este caso, serão os elementos amostrais são indivíduos.

Como a métrica de interesse é o *custo de processamento* dos indivíduos dentro dos grupos `Controle` e `Experimento`, vamos avaliar o comportamento do _tempo de reação_, _duração das fixações_, _número de fixações_ e _acurácia_ em função das variáveis delimitadas pelo estudo.

Assim, vamos considerar um modelo dentro da classe de modelos não-lineares mistos para medidas repetidas
\[
y_{ij} = f(A_{ij}, B_{ij}, {\boldmath \beta}, {\bf b}) + \epsilon_{ij},\quad i=1,\dots, M, j = 1,\dots n_i,
\]
onde

* $y_{ij}$ representam as variáveis que descrevem o indivíduo dentro do experimento,
* $M$ é o número de indivíduos,
* $n_i$ é o número de observações para o $i$-ésimo indivíduo,
* $A_{ij}$ é uma matriz representando os _efeitos fixos_,
* $B_{ij}$ é uma matriz representando os _erros aleatórios_,
* $\epsilon_{ij}$ é uma variável aleatória que descreve um ruído aditivo (_erros aleatórios_)
* $f$ é uma função real diferenciável até pelo menos segunda ordem.

## Modelando o tempo de reação

### Modelo 1

O primeiro modelo avaliado foi o do (logaritmo do) tempo de reação em função do grupo, da imagem escolhida, e o indivíduo como efeito aleatório (para levar em consideração a não independência das medições):

````
logrt ~ grupo + image_chosen + (recording_session_label)
````

Após o ajuste deste modelo, cujos resultados estão na tabela \@ref(tab:fm11). Para este modelo:

* O efeito do grupo é significativo e positivo
* Dependendo do tipo de imagem escolhida, o efeito é significativo e negativo

```{r fm11}
#' https://stat.ethz.ch/pipermail/r-help/2006-May/094765.html
fm11 <- df1 |>
  mutate(
    logrt = log(rt)
  ) |>
  lmer(logrt ~ grupo + image_chosen + (1|recording_session_label),
       data = _)
summary(fm11)$coefficients |>
             as.data.frame() |>
             rownames_to_column(var="term") |>
             select(-c("df")) |>
             as_tibble() |>
             mutate(term = case_when(
               term == "(Intercept)" ~ "Intercepto",
               term == "grupoExperimental" ~ "Grupo Experimental",
               term == "image_chosendistractor" ~ "Imagem Distratora",
               term == "image_chosentarget" ~ "Imagem Alvo",
               term == "image_chosenunrelated" ~ "Imagem Não Relacionada",
               TRUE ~ term
             )) |>
             rename(
               "Parâmetro" = "term",
               "Estimativa" = "Estimate",
               "Erro Padrão" = "Std. Error",
               "Valor-t" = "t value",
               "Valor-p" = "Pr(>|t|)"
             ) |>
             print_tbl(caption="Efeitos Fixos para o Tempo de reação modelado pelo Grupo e pelo Tipo de Imagem")
```

### Modelo 2

````
logrt ~ grupo + (1|recording_session_label),
````

Após o ajuste do segundo modelo, com resultados apresentados na tabela \@ref(tab:fm21), observou-se que o grupo ao qual o indivíduo pertence influencia no tempo de reação. Para a modelagem, também foi utilizado o logaritmo do tempo de reação. Assim, pelo intercepto, transformando para a escala original, o tempo de reação médio do grupo `Controle` é de $\mathrm{exp}(8,30301) = 4036,002\mathrm{ms}$. Já para a outra estimativa, nota-se que, $\mathrm{exp}(0.32858) = 1.388994$, evidenciando que o grupo `Experimental` tem um aumento no tempo de reação médio de aproximadamente $39\%$ em relação ao grupo `Controle`. Portanto, há evidências de que os letrados tem um tempo de reação menor que os não letrados.
```{r fm21}
#' https://stat.ethz.ch/pipermail/r-help/2006-May/094765.html
fm21 <- df1 |>
  mutate(
    logrt = log(rt)
  ) |>
  lmer(logrt ~ grupo + (1|recording_session_label),
       data = _)
summary(fm21)$coefficients |>
             as.data.frame() |>
             rownames_to_column(var="term") |>
             select(-c("df")) |>
             as_tibble() |>
             mutate(term = case_when(
               term == "(Intercept)" ~ "Intercepto",
               term == "grupoExperimental" ~ "Grupo Experimental",
               term == "image_chosendistractor" ~ "Imagem Distratora",
               term == "image_chosentarget" ~ "Imagem Alvo",
               term == "image_chosenunrelated" ~ "Imagem Não Relacionada",
               TRUE ~ term
             )) |>
             rename(
               "Parâmetro" = "term",
               "Estimativa" = "Estimate",
               "Erro Padrão" = "Std. Error",
               "Valor-t" = "t value",
               "Valor-p" = "Pr(>|t|)"
             ) |>
             print_tbl(caption="Efeitos Fixos para Tempo de reação modelado pelo Grupo")
```
Vale salientar que para os modelos avaliados, utilizou-se da adequação dos modelos decorrente de resultados assintóticos, tendo em vista a quantidade de observações presentes no conjunto de dados.

<!--
### Modelo 3

````
logrt ~ grupo + targetimage + (1|recording_session_label),
````

O terceiro modelo utiliza a imagem alvo como referência ao invés da imagem competidora. Os resultados são mostrados na tabela \@ref(tab:fm31).

```{r fm31}
#' https://stat.ethz.ch/pipermail/r-help/2006-May/094765.html
fm31 <- df1 |>
  mutate(
    logrt = log(rt)
  ) |>
  lmer(logrt ~ grupo + targetimage + (1|recording_session_label),
       data = _)
summary(fm31)$coefficients |>
             as.data.frame() |>
             rownames_to_column(var="term") |>
             select(-c("df")) |>
             as_tibble() |>
             mutate(term = case_when(
               term == "(Intercept)" ~ "Intercepto",
               term == "grupoExperimental" ~ "Grupo Experimental",
               term == "targetimageaborrecer_triE.png" ~ "Aborrecer",
               term == "targetimageafastar_AE.png" ~ "Afastar",
               term == "targetimagealegrar_AE.png" ~ "Alegrar",
               term == "targetimageamar_AE.png" ~ "Amar",
               term == "targetimageaproximar_AE.png" ~ "Aproximar",
               term == "targetimagearrastar_AE.png" ~ "Arrastar",
               term == "targetimagechutar_AE.png" ~ "Chutar",
               term == "targetimageempurrar_AE.png" ~ "Empurrar",
               term == "targetimageesmurrar_AE.png" ~ "Esmurrar",
               term == "targetimageodiar_AE.png" ~ "Odiar",
               term == "targetimagepuxar_AE.png" ~ "Puxar",
               term == "targetimagetrazer_AE.png" ~ "Trazer",
               TRUE ~ term
             )) |>
             rename(
               "Parâmetro" = "term",
               "Estimativa" = "Estimate",
               "Erro Padrão" = "Std. Error",
               "Valor-t" = "t value",
               "Valor-p" = "Pr(>|t|)"
             ) |>
             print_tbl(caption="Efeitos Fixos para o Tempo de reação modelado pelo Grupo e pelo Tipo de Imagem Alvo")
```
-->

## Modelando a duração das fixações

### Modelo 1

O segundo modelo avaliado foi para a duração das fixações, supondo que segue Distribuição Normal, em função do grupo e do tipo da imagem, contendo o indivíduo como efeito aleatório:

````
ia_dwell_time ~ grupo + image_chosen + (1|recording_session_label),
````

```{r fm12}
fm12 <- df1 |>
  lmer(ia_dwell_time ~ grupo + image_chosen + (1|recording_session_label),
       data = _)
summary(fm12)$coefficients |>
             as.data.frame() |>
             rownames_to_column(var="term") |>
             select(-c("df")) |>
             as_tibble() |>
             mutate(term = case_when(
               term == "(Intercept)" ~ "Intercepto",
               term == "grupoExperimental" ~ "Grupo Experimental",
               term == "image_chosendistractor" ~ "Imagem Distratora",
               term == "image_chosentarget" ~ "Imagem Alvo",
               term == "image_chosenunrelated" ~ "Imagem Não Relacionada",
               TRUE ~ term
             )) |>
             rename(
               "Parâmetro" = "term",
               "Estimativa" = "Estimate",
               "Erro Padrão" = "Std. Error",
               "Valor-t" = "t value",
               "Valor-p" = "Pr(>|t|)"
             ) |>
             print_tbl(caption="Efeitos Fixos para a Duração das Fixações modelado pelo Grupo e pelo Tipo de Imagem")
```

Os resultados da tabela \@ref(tab:fm12) indicam que a variável imagem escolhida não é significativa a um nível de $5\%$; assim, ajustando o modelo somente com a variável grupo,

````
ia_dwell_time ~ grupo + (1|recording_session_label),
````

temos a tabela \@ref(tab:fm22):

```{r fm22}
fm22 <- df1 |>
  lmer(ia_dwell_time ~ grupo + (1|recording_session_label),
       data = _)
summary(fm22)$coefficients |>
             as.data.frame() |>
             rownames_to_column(var="term") |>
             select(-c("df")) |>
             as_tibble() |>
             mutate(term = case_when(
               term == "(Intercept)" ~ "Intercepto",
               term == "grupoExperimental" ~ "Grupo Experimental",
               term == "image_chosendistractor" ~ "Imagem Distratora",
               term == "image_chosentarget" ~ "Imagem Alvo",
               term == "image_chosenunrelated" ~ "Imagem Não Relacionada",
               TRUE ~ term
             )) |>
             rename(
               "Parâmetro" = "term",
               "Estimativa" = "Estimate",
               "Erro Padrão" = "Std. Error",
               "Valor-t" = "t value",
               "Valor-p" = "Pr(>|t|)"
             ) |>
             print_tbl(caption="Efeitos Fixos para Duração das Fixações modelado pelo Grupo")
```
Nota-se que para os dois modelos, o grupo tem significância.

## Modelando o número de fixações

### Modelo 1

Avaliando o número de fixações em função do grupo e do tipo de imagem, supondo que o número de fixações
segue Distribuição Normal:

````
trial_fixation_count ~ grupo + image_chosen + (1|recording_session_label),
````

Com base nos resultados do modelo, mostrados na tabela \@ref(tab:fm13), podemos ver que a um nível de $5\%$ a imagem escolhida é significativa, e que o grupo influencia o número de fixações.

```{r fm13}
fm13 <- df1 |>
  lmer(trial_fixation_count ~ grupo + image_chosen + (1|recording_session_label),
       data = _)
summary(fm13)$coefficients |>
             as.data.frame() |>
             rownames_to_column(var="term") |>
             select(-c("df")) |>
             as_tibble() |>
             mutate(term = case_when(
               term == "(Intercept)" ~ "Intercepto",
               term == "grupoExperimental" ~ "Grupo Experimental",
               term == "image_chosendistractor" ~ "Imagem Distratora",
               term == "image_chosentarget" ~ "Imagem Alvo",
               term == "image_chosenunrelated" ~ "Imagem Não Relacionada",
               TRUE ~ term
             )) |>
             rename(
               "Parâmetro" = "term",
               "Estimativa" = "Estimate",
               "Erro Padrão" = "Std. Error",
               "Valor-t" = "t value",
               "Valor-p" = "Pr(>|t|)"
             ) |>
             print_tbl(caption="Efeitos Fixos para o Número de Fixações modelado pelo Grupo e pelo Tipo de Imagem")
```

### Modelo 2

````
trial_fixation_count ~ grupo + (1|recording_session_label)
````

```{r fm23}
fm23 <- df1 |>
  lmer(trial_fixation_count ~ grupo + (1|recording_session_label),
       data = _)
summary(fm23)$coefficients |>
             as.data.frame() |>
             rownames_to_column(var="term") |>
             select(-c("df")) |>
             as_tibble() |>
             mutate(term = case_when(
               term == "(Intercept)" ~ "Intercepto",
               term == "grupoExperimental" ~ "Grupo Experimental",
               term == "image_chosendistractor" ~ "Imagem Distratora",
               term == "image_chosentarget" ~ "Imagem Alvo",
               term == "image_chosenunrelated" ~ "Imagem Não Relacionada",
               TRUE ~ term
             )) |>
             rename(
               "Parâmetro" = "term",
               "Estimativa" = "Estimate",
               "Erro Padrão" = "Std. Error",
               "Valor-t" = "t value",
               "Valor-p" = "Pr(>|t|)"
             ) |>
             print_tbl(caption="Efeitos Fixos para o Número de Fixações modelado pelo Grupo e pelo Tipo de Imagem")
```

Vale salientar que para os modelos avaliados, utilizou-se da adequação dos modelos decorrente de resultados
assintóticos, tendo em vista a quantidade de observações presentes no conjunto de dados.

## Modelando a acurácia

### Modelo 1

Tem-se o interesse em avaliar se há diferença nos acertos entre pessoas letradas e não letradas. Para isso,
ajustou-se o modelo para a acurácia das respostas em função do grupo, supondo que a acurácia segue _Distribuição
Binomial_:

````
accuracy ~ grupo + (1|recording_session_label),
````

Os resultados estão na tabela \@ref(tab:fm14).

```{r fm14}
fm14 <- df1 |>
  mutate(
    acuracia = case_when(
      accuracy == "incorrect" ~ 0,
      accuracy == "correct" ~ 1,
      TRUE ~ NA
    )
  ) |>
  glmer(acuracia ~ grupo + (1|recording_session_label),
        data = _,
        family = "binomial")

summary(fm14)$coefficients |>
             as.data.frame() |>
             rownames_to_column(var="term") |>
             as_tibble() |>
             mutate(term = case_when(
               term == "(Intercept)" ~ "Intercepto",
               term == "grupoExperimental" ~ "Grupo Experimental",
               term == "image_chosendistractor" ~ "Imagem Distratora",
               term == "image_chosentarget" ~ "Imagem Alvo",
               term == "image_chosenunrelated" ~ "Imagem Não Relacionada",
               TRUE ~ term
             )) |>
             rename(
               "Parâmetro" = "term",
               "Estimativa" = "Estimate",
               "Erro Padrão" = "Std. Error",
               "Valor-z" = "z value",
               "Valor-p" = "Pr(>|z|)"
             ) |>
             print_tbl(caption="Efeitos Fixos para a acurária modelado pelo Grupo")
```
