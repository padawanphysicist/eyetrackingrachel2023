---
title: "Experimento 2"
output: 
  bookdown::pdf_document2:
    latex_engine: xelatex
    keep_tex: true
    toc_depth: 3
header-includes:
  - \usepackage{icomma}
  - \usepackage{float}
fontsize: 12pt
lang: pt-BR
geometry: margin=1in
vignette: >
  %\VignetteIndexEntry{experimento02}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
  %\VignetteDepends{tidyverse}
  %\VignetteDepends{devtools}
  %\VignetteDepends{lme4}
  %\VignetteDepends{lmerTest}
  %\VignetteDepends{broom.mixed}
  %\VignetteDepends{kableExtra}
  %\VignetteDepends{ggthemes}
  %\VignetteDepends{ggsci}
  %\VignetteDepends{rgl}
---

```{r, include = FALSE}
library(rgl)

knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  echo = FALSE,
  message = FALSE,
  warning = FALSE
)

options(rgl.useNULL = TRUE)
setupKnitr(autoprint = TRUE)
```

```{r setup}
devtools::load_all()
library(eyetrackingRachel2023)
library(tidyverse)
library(lme4)
library(lmerTest)
library(ggthemes)
library(ggsci)
library(broom.mixed)
library(kableExtra)

print_tbl <- function(x, caption = "", escape=T) {
  x |>
    knitr::kable(
      caption = caption,
      escape = escape,
      booktabs = TRUE
    ) |>
    kableExtra::kable_styling(
      latex_options = c("HOLD_position", "scale_down")
    )
}
```

# Introdução

Este relatório tem por objetivo auxiliar na investigação do comportamento dos grupos `Controle`, composto por indivíduos letrados, e `Experimental`, formado por indivíduos não letrados, com base no `Experimento 2`, focado na movimentação ocular vertical, especificamente as _sacadas_.

Busca-se compreender se a habilidade de ler da esquerda para a direita é um traço biológico ou predominantemente influenciada por fatores culturais. Para isso, serão analisados os resultados obtidos neste experimento e no [experimento 1](experimento01.pdf) para identificar padrões de comportamento.

O `Experimento 2` consiste em observar a movimentação ocular em quatro direções (cima, baixo, direita, esquerda) em resposta a estímulos sonoros ou visuais. O grupo `Experimental` irá ouvir o som de uma palavra, enquanto o grupo `Controle` visualizará uma palavra no centro da tela. A palavra fornecerá uma direção específica, e o teste analisará se a pessoa inicialmente direciona o olhar para a orientação indicada pela palavra. Posteriormente, uma imagem aparecerá em uma das quatro posições, e a pessoa deverá clicar sobre ela.

```{r}
#' Tabela a ser utilizada nas análises
df2 <- experimento02 |>
  left_join(
    participantes |>  
      filter(experimento == 2) |>
      select(id, grupo),
    by = c("recording_session_label" = "id"))
```

Para analisar o *custo de processamento* dentro do experimento. Por *custo de processamento*, entende-se uma das seguintes medidas:

* duração da sacada atual: coluna `current_sac_duration`,
* velocidade média da sacada atual: coluna `current_sac_avg_velocity`,
* tempo de reação: coluna `rt`.

Assim, temos as relações:

* duração da sacada atual maior/menor $\implies$ maior/menor custo de processamento
* velocidade média da sacada atual maior/menor $\implies$ menor/maior custo de processamento
* tempo de reação maior/menor $\implies$ maior/menor custo de processamento

# Análise Descritiva

Como o interesse é no movimento ocular vertical, selecionamos as observações em que a direção da sacada é _vertical_ ou _neutra_:

````
current_sac_direction %in% c("UP", "DOWN", "NEUTRAL")
````

também, definimos

* `pr_type == "control"` como `Forma Geométrica`,
* `pr_type == "opposite"` como `Não Relacionada`,
* `pr_type == "same_spatial"` como `Relacionada`,
* `pr_type == "semantic"` como `Relacionada`,

e também 

* `pr_spat_as == tr_location` como `Congruente`,
* `pr_spat_as != tr_location` como `Não Congruente`.

Na tabela \@ref(tab:desc-rt) tempos as medidas descritivas do tempo de reação. Observe que a média do tempo de reação é, em geral, menor para o grupo `Controle`. Na figura \@ref(fig:rt-teste) temos o tempo de reação por teste. Pode-se observar que o grupo `Experimental` apresenta um tempo de reação maior do que o grupo `Controle`, e portanto um maior custo de processamento.

Na figura \@ref(fig:rt-teste) também podemos ver que o tempo reação se estabiliza ao longo do experimento.

```{r desc-rt}
tbl1 <- function() {
  df2 |>
  mutate(
    condition = case_when(
      pr_type == "control" ~ "Forma Geométrica",
      pr_type == "opposite" ~ "Não Relacionada",
      pr_type == "same_spatial" ~ "Relacionada",
      pr_type == "semantic" ~ "Relacionada",
      TRUE ~ NA
    ),
    congruencia = case_when(
      pr_spat_as == tr_location ~ "Congruente",
      pr_spat_as != tr_location ~ "Não Congruente",
      TRUE ~ NA
    )
  ) |>  
  filter(current_sac_direction %in% c("UP", "DOWN", "NEUTRAL")) |>
  filter(!is.na(current_sac_direction)) |>
  group_by(grupo, condition, congruencia) |>
  summarize(
    Media = mean(rt),
    Variancia = var(rt),
    `Desvio Padrão` = sd(rt)
  ) |>
  ungroup() |>
  pivot_wider(names_from = "grupo", values_from = c(Media, Variancia, `Desvio Padrão`)) |>
    rename(
      "Condição" = "condition",
      "Congruência" = "congruencia",
      "Média_c" = "Media_Controle",
      "Var_c" = "Variancia_Controle",
      "Dev_c" = "Desvio Padrão_Controle",
      "Média_e" = "Media_Experimental",
      "Var_e" = "Variancia_Experimental",
      "Dev_e" = "Desvio Padrão_Experimental"
    )
}

tbl1() |>
  print_tbl(caption = "Medidas Descritivas do tempo de reação o tipo de prime e associação do prime")
```

```{r rt-teste, fig.cap="Tempo de reação por teste", fig.show = "hold", fig.dim = c(9, 5)}
df2 |>
  group_by(trial_index, grupo) |>
  summarize(media_tempo_reacao = mean(rt)) |>
  ungroup() |>
  ggplot(aes(x = trial_index, y = media_tempo_reacao, color = grupo)) +
  geom_point() +
  stat_smooth() +
  labs(
    x = "Teste",
    y = "Média de tempo de reação (ms)",
    color = "Grupo"
  ) +
  theme_economist() + scale_colour_economist() +
  theme(
    axis.title.y = element_text(vjust = +3.0),
    axis.title.x = element_text(vjust = -2.0)
  ) 
```

Na tabela \@ref(tab:desc-velocidade-sac-atual) temos as medidas descritivas para a velocidade da sacada atual. Podemos ver que o grupo `Controle` possui uma velocidade de sacada maior do que o grupo `Experimental`:

```{r desc-velocidade-sac-atual}
tbl2 <- function() {
  df2 |>
    filter(current_sac_direction %in% c("UP", "DOWN", "NEUTRAL")) |>
    filter(!is.na(current_sac_direction)) |>
    group_by(grupo) |>
    summarize(
      `Média` = mean(current_sac_avg_velocity, na.rm=T),
      `Variância` = var(current_sac_avg_velocity, na.rm=T),
      `Desvio Padrão` = sd(current_sac_avg_velocity, na.rm=T)
    ) |>
    ungroup() |>
    rename("Grupo" = "grupo")
}

tbl2() |>
  print_tbl(caption = "Medidas Descritivas da Velocidade Média da Sacada Atual por grupo.")
```
Pela figura \@ref(fig:vel-sac-teste) podemos também ver que a diferença entre as velocidades médias das sacadas é maior na fase inicial do teste, se estabilizando no decorrer.

```{r vel-sac-teste, fig.cap="Média da velocidade da sacada por teste", fig.show = "hold", fig.dim = c(9, 5)}
df2 |>
  group_by(trial_index, grupo) |>
  summarize(media_vel = mean(current_sac_avg_velocity, na.rm=T)) |>
  ungroup() |>
  ggplot(aes(x = trial_index, y = media_vel, color = grupo)) +
  geom_point() +
  stat_smooth() +
  labs(
    x = "Teste",
    y = "Média da velocidade da sacada",
    color = "Grupo"
  ) +
  theme_economist() + scale_colour_economist() +
  theme(
    axis.title.y = element_text(vjust = +3.0),
    axis.title.x = element_text(vjust = -2.0)
  ) 
```

Vamos também analisar o _comprimento da sacada_, que definimos como sendo o deslocamento absoluto na direção y: 
````
comprimento_sacada = abs(current_sac_end_y - current_sac_start_y).
````

na tabela \@ref(tab:desc-comprimento-sac-atual) temos as medidas descritivas para o comprimento da sacada por grupo. O grupo `Controle` apresenta um comprimento de sacada maior, e isso permanece ao longo de todo o experimento, como indica a figura \@ref(fig:compr-sac-teste).

```{r desc-comprimento-sac-atual}
df2 |>
  mutate(
    comprimento_sacada = abs(current_sac_end_y - current_sac_start_y)
  ) |>
  mutate(
    condition = case_when(
      pr_type == "control" ~ "geometric shape",
      pr_type == "opposite" ~ "unrelated",
      pr_type == "same_spatial" ~ "semantically related",
      pr_type == "semantic" ~ "semantically related",
      TRUE ~ NA
    ),
    congruencia = case_when(
      pr_spat_as == tr_location ~ "congruent",
      pr_spat_as != tr_location ~ "incongruent",
      TRUE ~ NA
    )
  ) |>  
  filter(current_sac_direction %in% c("UP", "DOWN", "NEUTRAL")) |>
  filter(!is.na(current_sac_direction)) |>
  group_by(grupo) |> #, condition, congruencia) |>
  summarize(
    `Média` = mean(comprimento_sacada, na.rm=T),
    `Variância` = var(comprimento_sacada, na.rm=T),
    `Desvio Padrão` = sd(comprimento_sacada, na.rm=T)
  ) |>
  ungroup() |>
  print_tbl(caption = "Medidas Descritivas comprimento da Sacada Atual por grupo.")
```

```{r compr-sac-teste, fig.cap="Média do comprimento de sacada por teste", fig.show = "hold", fig.dim = c(9, 5)}
df2 |>
  mutate(
    comprimento_sacada = abs(current_sac_end_y - current_sac_start_y)
  ) |>
  group_by(trial_index, grupo) |>
  summarize(media_comp = mean(comprimento_sacada, na.rm=T)) |>
  ungroup() |>
  ggplot(aes(x = trial_index, y = media_comp, color = grupo)) +
  geom_point() +
  stat_smooth() +
  labs(
    x = "Teste",
    y = "Média do comprimento da sacada",
    color = "Grupo"
  ) +
  theme_economist() + scale_colour_economist() +
  theme(
    axis.title.y = element_text(vjust = +3.0),
    axis.title.x = element_text(vjust = -2.0)
  ) 
```

# Análise Longitudinal

Será feita agora uma análise longitudinal nos mesmos moldes do [experimento 1](experimento01.pdf)

Como a métrica de interesse é o *custo de processamento* dos indivíduos dentro dos grupos `Controle` e `Experimento`, vamos avaliar o comportamento do _tempo de reação_, _duração da sacada atual_, _velocidade média da sacada atual_ e _comprimento da sacada_ em função das variáveis delimitadas pelo estudo.

## Modelando o tempo de reação

### Modelo 1

Foram excluídas as observações para as quais `rt < 200`. A partir da tabela \@ref(tab:fm11) podemos concluir que o efeito do grupo não é significativo neste caso.

````
logrt ~ grupo + (1|recording_session_label)
````

```{r fm11}
fm11 <- df2 |>
  filter(rt >= 200) |>
  filter(current_sac_direction %in% c("UP", "DOWN", "NEUTRAL")) |>
  mutate(
    logrt = log(rt)
  ) |>
  lmer(logrt ~ grupo + (1|recording_session_label),
       data = _)
summary(fm11)$coefficients |>
             as.data.frame() |>
             rownames_to_column(var="term") |>
             select(-c("df")) |>
             as_tibble() |>
             mutate(term = case_when(
               term == "(Intercept)" ~ "Intercepto",
               term == "grupoExperimental" ~ "Grupo Experimental",
               TRUE ~ term
             )) |>
             rename(
               "Parâmetro" = "term",
               "Estimativa" = "Estimate",
               "Erro Padrão" = "Std. Error",
               "Valor-t" = "t value",
               "Valor-p" = "Pr(>|t|)"
             ) |>
             print_tbl(caption="Efeitos Fixos para o tempo de reação modelado pelo Grupo")
```

## Modelando a duração da sacada atual

### Modelo 1

````
 current_sac_duration ~ grupo + pr_spat_as + (1|recording_session_label)
````

```{r fm21}
fm21 <- df2 |>
  lmer(current_sac_duration ~ grupo + (1|recording_session_label),
       data = _)
summary(fm21)$coefficients |>
             as.data.frame() |>
             rownames_to_column(var="term") |>
             select(-c("df")) |>
             as_tibble() |>
             mutate(term = case_when(
               term == "(Intercept)" ~ "Intercepto",
               term == "grupoExperimental" ~ "Grupo Experimental",
               TRUE ~ term
             )) |>
             rename(
               "Parâmetro" = "term",
               "Estimativa" = "Estimate",
               "Erro Padrão" = "Std. Error",
               "Valor-t" = "t value",
               "Valor-p" = "Pr(>|t|)"
             ) |>
             print_tbl(caption="Efeitos Fixos para o duração da sacada modelado pelo grupo")
```

## Modelando a velocidade média da sacada atual
### Modelo 1

````
 current_sac_avg_velocity ~ grupo + pr_spat_as + (1|recording_session_label)
````

```{r fm31}
fm31 <- df2 |>
  lmer(current_sac_avg_velocity ~ grupo + (1|recording_session_label),
       data = _)
summary(fm31)$coefficients |>
             as.data.frame() |>
             rownames_to_column(var="term") |>
             select(-c("df")) |>
             as_tibble() |>
             mutate(term = case_when(
               term == "(Intercept)" ~ "Intercepto",
               term == "grupoExperimental" ~ "Grupo Experimental",
               TRUE ~ term
             )) |>
             rename(
               "Parâmetro" = "term",
               "Estimativa" = "Estimate",
               "Erro Padrão" = "Std. Error",
               "Valor-t" = "t value",
               "Valor-p" = "Pr(>|t|)"
             ) |>
             print_tbl(caption="Efeitos Fixos para o velocidade média da sacada modelado pelo grupo")
```

## Modelando o comprimento da sacada atual
### Modelo 1
Na tabela \@ref(tab:fm41) temos os resultados para o modelo de predição do comprimento da sacada pelo grupo, segundo a fórmula

````
 comprimento_sacada ~ grupo + (recording_session_label)
````

Neste modelo podemos concluir que o efeito do grupo é significativo, e assim vamos explorar a influência de outras variáveis.

```{r fm41}
fm41 <- df2 |>
  mutate(
    comprimento_sacada = abs(current_sac_end_y - current_sac_start_y)
  ) |>
  lmer(comprimento_sacada ~ grupo + (1|recording_session_label),
       data = _)
summary(fm41)$coefficients |>
             as.data.frame() |>
             rownames_to_column(var="term") |>
             select(-c("df")) |>
             as_tibble() |>
             mutate(term = case_when(
               term == "(Intercept)" ~ "Intercepto",
               term == "grupoExperimental" ~ "Grupo Experimental",
               TRUE ~ term
             )) |>
             rename(
               "Parâmetro" = "term",
               "Estimativa" = "Estimate",
               "Erro Padrão" = "Std. Error",
               "Valor-t" = "t value",
               "Valor-p" = "Pr(>|t|)"
             ) |>
             print_tbl(caption="Efeitos Fixos para o comprimento da sacada modelado pelo grupo")
```

### Modelo 2

Ajustamos um modelo linear misto para prever o comprimento da sacada:

````
comprimento_sacada ~ grupo + pr_spat_as + pr_type + tr_location + 
                     (1|recording_session_label)
````
O poder explicativo total do modelo é fraco ($R^2 = 0,05$) e a parte relativa apenas aos efeitos fixos ($R^2$ marginal) é de $0,02$. Na tabela \@ref(tab:fm42) temos os resultados para o modelo. Dentro desse modelo, podemos concluir que os efeitos das variáveis é significativo

```{r fm42}
fm42 <- df2 |>
  mutate(
    comprimento_sacada = abs(current_sac_end_y - current_sac_start_y)
  ) |>
  lmer(comprimento_sacada ~ grupo + pr_spat_as + pr_type + tr_location + (1|recording_session_label),
       data = _) 
summary(fm42)$coefficients |>
             as.data.frame() |>
             rownames_to_column(var="term") |>
             select(-c("df")) |>
             as_tibble() |>
             mutate(term = case_when(
               term == "(Intercept)" ~ "Intercepto",
               term == "grupoExperimental" ~ "Grupo Experimental",
               TRUE ~ term
             )) |>
             rename(
               "Parâmetro" = "term",
               "Estimativa" = "Estimate",
               "Erro Padrão" = "Std. Error",
               "Valor-t" = "t value",
               "Valor-p" = "Pr(>|t|)"
             ) |>
             print_tbl(caption="Efeitos Fixos para o velocidade média da sacada modelado pelo grupo e tipo de prime")
```

### Modelo 3

Ajustamos um modelo linear misto para prever o comprimento da sacada, mas considerando também a interação entre a associação espacial do prime e a localização do alvo:

````
comprimento_sacada ~ grupo + pr_spat_as*tr_location + pr_type +
                     (1|recording_session_label)
````
Na tabela \@ref(tab:fm43) temos os resultados para o modelo.

```{r fm43}
fm43 <- df2 |>
  mutate(
    comprimento_sacada = abs(current_sac_end_y - current_sac_start_y)
  ) |>
  lmer(comprimento_sacada ~ grupo + pr_spat_as*tr_location + pr_type + (1|recording_session_label),
       data = _) 
summary(fm43)$coefficients |>
             as.data.frame() |>
             rownames_to_column(var="term") |>
             select(-c("df")) |>
             as_tibble() |>
             mutate(term = case_when(
               term == "(Intercept)" ~ "Intercepto",
               term == "grupoExperimental" ~ "Grupo Experimental",
               TRUE ~ term
             )) |>
             rename(
               "Parâmetro" = "term",
               "Estimativa" = "Estimate",
               "Erro Padrão" = "Std. Error",
               "Valor-t" = "t value",
               "Valor-p" = "Pr(>|t|)"
             ) |>
             print_tbl(caption="Efeitos Fixos para o velocidade média da sacada modelado pelo grupo e tipo de prime")
```


